FROM node:8-alpine
WORKDIR /build
COPY package*.json ./
RUN npm install
COPY . ./
RUN npm run build

FROM nginx:alpine
WORKDIR /usr/share/nginx/html
COPY --from=0 /build/build/ .
COPY nginx.conf /etc/nginx/nginx.conf
