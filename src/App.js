import React, { Component } from 'react';
import { HashRouter, Route, Switch } from 'react-router-dom';
import './App.scss';

// Containers
import { DefaultLayout } from './containers';
// Pages
import { Login, Page404, Page500, Register } from './views/Pages';

// import { renderRoutes } from 'react-router-config';
import { Notifications } from './notifications/notifications';
class App extends Component {
  render() {
    return (
      <div>
      <HashRouter>
        <Switch>
          <Route exact path="/login" name="Strona logowania" component={Login} />
          <Route exact path="/register" name="Rejestracja konta" component={Register} />
          <Route exact path="/404" name="Page 404" component={Page404} />
          <Route exact path="/500" name="Page 500" component={Page500} />
          <Route path="/" name="Home" component={DefaultLayout} />
        </Switch>
      </HashRouter>
      <Notifications></Notifications>
      
      </div>
      );
  }
}

export default App;
