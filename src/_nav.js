export default {
  items: [
    {
      name: 'Dashboard',
      url: '/dashboard',
      icon: 'icon-speedometer',
      badge: {
        variant: 'info',
        text: 'HOME',
      },
      anonymous: true
    },
    {
      title: true,
      name: 'User',
      anonymous: true
    },
    {
      name: 'Played games',
      url: '/my-games',
      icon: 'icon-clock',
      anonymous: true
    },
    {
      name: 'Find your game',
      url: '/list',
      icon: 'icon-user',
      anonymous: true
    },
    // { 
    //   name: 'Statystyki',
    //   url: '/stats',
    //   icon: 'icon-clock',
    //   anonymous: true
    // }
    {
      title: true,
      name: 'Admin',
      // anonymous: true,
      roles: [
        'ADMIN',
      ]
    },
    {
      name: 'New game',
      url: '/new-game',
      icon: 'icon-like',
      // anonymous: true,
      roles: [
        'ADMIN'
      ]
    }
    
  ],
};
