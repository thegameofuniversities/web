import React, {Component} from 'react';
import Quagga from 'quagga';
import {Button} from "reactstrap";
import PropTypes from "prop-types"; // ES6
import { Rest } from '../../services/rest.service';



class BarcodeReader extends Component {

  render() {
    return (
      <div>
        <input type="file" accept="image/*;capture=camera" style={{ display: 'none' }} ref={(ref) => this.upload = ref} onChange={ (e) => this.decode(e.target.files) }/>
        <Button type="button" className="btn btn-block btn-success mb-2" onClick={()=>this.upload.click()}> Scan barcode </Button>
      </div>
    );
  }

  decode(files) {

    let config = {
      decoder: {
        readers: ["ean_reader"] // List of active readers
      },
      locate: true, // try to locate the barcode in the image
      src: URL.createObjectURL(files[0]), // or 'data:image/jpg;base64,' + data
      debug: true
    }

    Quagga.decodeSingle(config, (result) => {
      Rest.getByEan(result.codeResult.code).then((res) => {
        this.redirect(`/game/${res.id}`)
      });
    });

  }

  static contextTypes = {
    router: PropTypes.object
  }
  redirect = (target) => {
    this.context.router.history.push(target);
  }
}

export default BarcodeReader;
