import React, { Component } from 'react';
import { Button } from 'reactstrap';



class BookingButton extends Component {

  render() {
    return (
      <>
      {this.props.available && <Button className={this.props.classes} color="dark" onClick={this.props.clicked}>Ready to reserve!</Button>}
      {this.props.available || <Button className={this.props.classes} disabled color="dark">You can not reserve this game today :(</Button>}
      </>
    );
  }
}

export default BookingButton;