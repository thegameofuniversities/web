import React, { Component } from 'react';
import { Card, CardBody, Col, Row } from 'reactstrap';
import moment from 'moment';
import Day from './Day/Day.js';
import { Rest } from '../../services/rest.service.js';
import { connect } from 'react-redux';
import { addNotification } from '../../redux/actions';

class Calendar extends Component {

  constructor() {
    super();
    this.state = {
      month: [],
      reserved: [],
      confirmed: [],
      unavailable: [],
      preemtable: [],
    }
  }

  componentDidUpdate(prev) {
    if(this.props.month !== prev.month || this.props.year !== prev.year) {
      this.crateDays();
      this.updatePage();
    }
  }

  updatePage = () => {
    Rest.getMonth(this.props.id, this.props.month, this.props.year)
      .then(e => {
        this.setState({
          ...this.state,
          ...e,
        })
      })
  }

  componentDidMount() {
    this.updatePage();
    this.crateDays();
  }

  crateDays = () => {
    let date = new moment(`${this.props.year}-${this.props.month}-10`);
    date = date.subtract(date.date(),'d');
    date = date.subtract(date.day(), 'd');
    const days = []
    while(date.date() !== 1 || (date.month() !== this.props.month || date.month() === 0 && this.props.mont !== 1)) {
      date = date.add(1,'d');
      if((date.date() !== 1 || (date.month() !== this.props.month || date.month() === 0 && this.props.mont !== 1))){
        days.push({
          dayOfWeek: date.day(),
          day: date.date(),
          month: date.month()+1,
          year: date.year(),
        });
      }
    }
    date = date.subtract(1, 'd');
    while(date.day() !== 0) {
      date = date.add(1,'d');
      days.push({
        dayOfWeek: date.day(),
        day: date.date(),
        month: date.month()+1,
        year: date.year(),
      });
    }
    const month = []
    let day = []
    days.forEach(d => {
      day.push(d);
      if(day.length === 7) {
        month.push(day);
        day = [];
      }
    });
    month.push(day);
    this.setState({month})
  }

  clicked = (day, my) => {
    if(!this.props.editable) {
      return;
    }
    if (my) {
      // const days = [day.day];
      // if(day.dayOfWeek === 5) {
      //   days.push(day.day + 1);
      //   days.push(day.day + 2)
      // }
      // if(day.dayOfWeek === 6) {
      //   days.push(day.day + 1);
      //   days.push(day.day - 1);
      // }
      // if(day.dayOfWeek === 0) {
      //   days.push(day.day - 2);
      //   days.push(day.day - 1);
      // }
      // this.setState({
      //   ...this.state,
      //   my: this.state.my.filter(d => !days.includes(d))
      // });
      
      // // todo rest
    } else {
      let start = new moment(new Date(day.year, day.month-1, day.day))
      let end = new moment(new Date(day.year, day.month-1, day.day)).add(1,'d')

      const days = [day.day];
      if(day.dayOfWeek === 5) {
        days.push(day.day + 1);
        days.push(day.day + 2)
        end = new moment(new Date(day.year, day.month-1, day.day)).add(1,'d');
      }
      if(day.dayOfWeek === 6) {
        days.push(day.day + 1);
        days.push(day.day - 1);
        start = new moment(new Date(day.year, day.month-1, day.day)).subtract(1,'d');
        end = new moment(new Date(day.year, day.month-1, day.day)).add(2,'d');
      }
      if(day.dayOfWeek === 0) {
        days.push(day.day - 2);
        days.push(day.day - 1);
        start = new moment(new Date(day.year, day.month-1, day.day)).subtract(2,'d');
        end = new moment(new Date(day.year, day.month-1, day.day)).add(1,'d');
      }
      start = new moment(start).add(11,'h').unix();
      end = new moment(end).add(10,'h').unix();
      Rest.book({
        games: [this.props.id],
        start,
        end
      }).then(e => {
        this.setState({
          ...this.state,
          reserved: [...this.state.reserved, ...days].filter(d => !this.state.unavailable.includes(d)),
        });
        
        this.props.addNotification({ type: 'info', message: 'Reserved!'});
      }).catch(e => {
        this.props.addNotification({ type: 'danger', message: 'Try again later!'});
      });
    }
  }

  getColor = (d) => {
    if (this.state.confirmed.includes(d)) {
      return 'success';
    }
    if (this.state.reserved.includes(d)) {
      return 'danger';
    }
    if (this.state.preemtable.includes(d)) {
      return 'warning';
    }
    if (this.state.unavailable.includes(d)) {
      return 'secondary';
    }
    return 'primary';
  }
  render() {
    return (
      
      <Card>
        <CardBody>
          <Row>
            <Col onClick={() => this.props.prev()}>
              <Card><CardBody className="bg-primary text-center font-xl py-2">Previous month</CardBody></Card>
            </Col>
            <Col>
            </Col>
            <Col onClick={() => this.props.next()}>
              <Card><CardBody className="bg-primary text-center font-xl py-2">Next month</CardBody></Card></Col>
          </Row>
            {this.state.month.map((m,i) => {
              return (<Row className="m-0 p-0" key={i}>
                {m.map((d,i) => {
                  return (
                    <Col className="m-1 p-0" key={i}>
                      <Day {...d}
                        editable={(!this.state.unavailable.includes(d.day)
                          && !this.state.reserved.includes(d.day)
                          && !this.state.confirmed.includes(d.day)) && d.month === this.props.month}
                        color={d.month !== this.props.month ? 'secondary' : this.getColor(d.day) }
                        onClick={() =>this.clicked(d, this.state.reserved.includes(d.day))}
                      ></Day></Col>
                  );
                })}
              </Row>);
            })}
            <Row className="mt-3">
              <Col>
                <Card><CardBody className="py-1 bg-danger text-center">Reserved</CardBody></Card>
              </Col>
              <Col>
                <Card><CardBody className="py-1 bg-success text-center">Confirmed</CardBody></Card>
              </Col>
              <Col>
                <Card><CardBody className="py-1 bg-secondary text-center">Unavailable</CardBody></Card>
              </Col>
              <Col>
                <Card><CardBody className="py-1 bg-warning text-center">Reserved, but you can steal it!</CardBody></Card>
              </Col>
              <Col>
                <Card><CardBody className="py-1 bg-primary text-center">Available</CardBody></Card>
              </Col>
            </Row>
        </CardBody>
      </Card>);
  }
}


const mapStateToProps = (state) => {
  return {};
};

const mapDispatchToProps = { addNotification }

export default Calendar = connect(mapStateToProps, mapDispatchToProps)(Calendar);
