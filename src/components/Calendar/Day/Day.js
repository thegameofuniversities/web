import React, { Component } from 'react';
import { Card, CardBody, Col, Row} from 'reactstrap';

import './style.css';
class Day extends Component {

  clicked = () => {
    if(this.props.editable) {
      this.props.onClick();
    }
  }

  render() {
    const months = ['Sty','Lut','Mar','Kwi','Maj','Cze','Lip','Sie','Wrz','Paź','Lis','Gru']
    return (
      <>
      <Card className={'m-0 p-0 ' + (this.props.editable ? 'clickable' : '')}
        onClick={this.clicked}  
      >
        <CardBody className={'bg-' + this.props.color}>
          <Row>
            <Col>
              <div>{this.props.day}</div>
              <div>{months[this.props.month-1]}</div>
              <div className="font-md">{this.props.year}</div>
            </Col>
            <Col>
              {this.props.editable && <i className="fa fa-calendar-plus-o fa-lg mt-4 front-3x1"></i>}
            </Col>
          </Row>
          </CardBody>
      </Card>
    </>);
  }
}


export default Day;