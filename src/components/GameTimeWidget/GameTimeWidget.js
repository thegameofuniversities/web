import React, { Component } from 'react';
import { Card, CardBody, CardFooter, CardHeader, Col, Row } from 'reactstrap';
import classNames from 'classnames';
import { mapToCssModules } from 'reactstrap/lib/utils';

import './GameTimeWidget.css';

class GameTimeWidget extends Component {
  render() {
    const { className, cssModule, title, description, image, color, footer, link, children, variant, ...attributes } = this.props;

    const card = { style: 'clearfix', color: color, image: image, classes: '' };
    card.classes = mapToCssModules(classNames(className, card.style, "p-0"), cssModule);


    const blockIcon = function (image) {
      return (<img className="game-img-full" src={image} alt="test"></img>)
    };

    const cardFooter = () => {
        return (<>
          <CardFooter className="px-3 py-2">
            <Row>
              <Col xs={12} md={6} xl={6}>
                From: <br/><b>{new Date(this.props.start*1000).toISOString().substring(0,10)}</b>
              </Col>
              <Col xs={12} md={6} xl={6}>
                To: <br/><b>{new Date(this.props.start*1000).toISOString().substring(0,10)}</b>
              </Col>
            </Row>
          </CardFooter>
        <CardFooter className="px-3 py-2">
          <a className="font-weight-bold font-xs btn-block text-muted pointer" href={"/#/game/"+ this.props.id}>Szczegóły gry
            <i className="fa fa-angle-right float-right font-lg"></i></a>
        </CardFooter>
      </>);
    };

    return (
      <Card>
        <CardHeader><i>{title}</i></CardHeader>
        <CardBody className={card.classes} {...attributes}>
          {blockIcon(card.image)}
        </CardBody>
        {cardFooter()}
      </Card>);
  }
}


export default GameTimeWidget;