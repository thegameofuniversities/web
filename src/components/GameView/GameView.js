import React, { Component } from 'react';
import { Card, CardBody, Col, Row, CardHeader } from 'reactstrap';
import BookingButton from '../BookingButton/BookingButton';
import Calendar from '../Calendar/Calendar';
import './GameView.css';

class GameView extends Component {
  
  constructor(){
    super();
    this.state = {
      year: 2019,
      month: 3,
    }
  }

  renderImage = (icon) => {
    return (<img className="m-2 view-img" src={this.props.image} alt="test"></img>)
  };

  componentDidMount() {
    this.setState({
      month: this.props.month,
      year: this.props.year
    });
  }

  componentDidUpdate(prev) {
    if(prev.month !== this.props.month || this.props.year !== prev.year) {
      this.setState({
        month: this.props.month,
        year: this.props.year
      });
    }
  }

  prevMonth = () => {
    this.setState({
      year: this.state.year === 0 ? this.state.year - 1 : this.state.year,
      month: this.state.month === 1 ? 12 : this.state.month - 1,
    });
  }

  nextMonth = () => {
    this.setState({
      year: this.state.month === 12 ? this.state.year + 1 : this.state.year,
      month: this.state.month === 12 ? 0 : this.state.month + 1,
    });
  }

  clicked =() => {
    const calendar = document.getElementById("calendar");
    if(calendar) {
      calendar.scrollIntoView({behavior: 'smooth' });
    }
  }

  render() {
    return (
      <>
      <Card>
        <CardBody>
          <Row>
            <Col xl={6} md={6} xs={6}>
              {this.renderImage()}
            </Col>
            <Col xl={6} md={6} xs={6}>
              <div className="h1 mt-4">{this.props.title}</div>
              <div className="text-muted font-md mt-2">{this.props.description}</div>
              {this.props.link && <div><a href={this.props.link}>Szczegółowe infromacje o grze</a></div>}
              {this.props.loggedUser && <BookingButton classes="mt-3" available={this.props.available} clicked={this.clicked}></BookingButton>}
            </Col>
          </Row>
        </CardBody>
      </Card>
      {this.props.id && <Card className="text-center" id="calendar">
        <CardHeader>
        <h1>Reservations section</h1>
        </CardHeader>
        <CardBody>
          <Calendar editable={!!this.props.loggedUser} year={this.state.year || 2019}  id={this.props.id} month={this.state.month || 3} prev={this.prevMonth} next={this.nextMonth}></Calendar>
        </CardBody>
      </Card>}
      </>
    );
  }
}

export default GameView;