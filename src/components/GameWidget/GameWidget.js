import React, { Component } from 'react';
import { Card, CardBody, CardFooter, Col, Row } from 'reactstrap';
import classNames from 'classnames';
import { mapToCssModules } from 'reactstrap/lib/utils';

import './GameWidget.css';


class GameWidget extends Component {
  render() {
    const { className, cssModule, title, description, image, color, footer, link, children, variant, ...attributes } = this.props;

    // demo purposes only
    const padding = (variant === '0' ? { card: 'p-3', icon: 'p-3', lead: 'mt-2' } : (variant === '1' ? {
      card: 'p-0', icon: 'p-4', lead: 'pt-3',
    } : { card: 'p-0', icon: 'p-4 px-5', lead: 'pt-3' }));

    const card = { style: 'clearfix', color: color, image: image, classes: '' };
    card.classes = mapToCssModules(classNames(className, card.style, padding.card), cssModule);

    const lead = { style: 'h5 mb-0', color: color, classes: '' };
    lead.classes = classNames(lead.style, 'text-' + card.color, padding.lead);

    const blockIcon = function (image) {
      const classes = classNames('bg-' + card.color, 'game-img m-2 float-left');
      return (<img className={classes} src={image} alt="test"></img>)
    };

    const cardFooter = function () {
      if (footer) {
        return (
          <CardFooter className="px-3 py-2">
            <a className="font-weight-bold font-xs btn-block text-muted pointer" href={link}>Details
              <i className="fa fa-angle-right float-right font-lg"></i></a>
          </CardFooter>
        );
      }
    };

    return (
      <Card>
        <CardBody className={card.classes} {...attributes}>
        <Row>
          <Col>
            {blockIcon(card.image)}
            <div className={lead.classes}>{title}</div>
          </Col>
          </Row>
          <Row>
          <Col className="mx-2">
            <div className="text-muted font-xs mb-2">{description}</div>
          </Col>
          </Row>
        </CardBody>
        {cardFooter()}
      </Card>
    );
  }
}

export default GameWidget;