import React, { Component } from 'react';
import { PaginationItem, Pagination, PaginationLink } from 'reactstrap';

export class PaginationComponent extends Component {

  pages = () => {
    const numbers = [];
    const { page } = this.props;
    for(let i = Math.max(1,page - 2); i<= Math.min(page + 2, this.maxPages); ++i) {
      numbers.push(i);
    }
    return numbers;
  }
  
  handleClick = (e, page) => {
    e.preventDefault();
    if(page !== this.props.page) {
        this.props.onChange(page);
    }
  }

  render() {
    const {total, size, page, onChange} = this.props;
    this.maxPages = Math.ceil(total / (size === 0 ? 1 : size));
    // todo do component did mount ??
    if(page<1) {
        onChange(1);
    } else if(page > this.maxPages) {
        onChange(this.maxPages);
    }
    return (
      <div className="animated fadeIn">
        <Pagination>
            {
                this.props.page > 1 && 
                <PaginationItem>
                    <PaginationLink previous tag="button" onClick={(e) => this.handleClick(e, 1)} />
                </PaginationItem>
            }
            {
                this.pages().map(num =>
                    <PaginationItem key={num} active={num === this.props.page}>
                        <PaginationLink tag="button" onClick={(e) => this.handleClick(e, num)}>
                            {num}
                        </PaginationLink>
                    </PaginationItem>        
                )
            }
            {
                this.maxPages > this.props.page &&
                <PaginationItem >
                    <PaginationLink next tag="button" onClick={(e) => this.handleClick(e, this.maxPages)}/>
                </PaginationItem>
            }
        </Pagination>
      </div>
    );
  }
}
