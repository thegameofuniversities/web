import React, { Component } from 'react';
import Autosuggest from 'react-autosuggest';
import {
  Input,
} from 'reactstrap';
import './style.css';

class Sugestion extends Component {
  constructor(){
    super();
    this.state = {
      value: '',
      sugestions: []
    };
  }

  escapeRegexCharacters = (str) => {
    return str.replace(/[.*+?^${}()|[\]\\]/g, '\\$&');
  }
  
  getSuggestions = (value) => {
    const escapedValue = this.escapeRegexCharacters(value.trim());
    
    if (escapedValue === '') {
      return [];
    }
  
    const regex = new RegExp(escapedValue, 'i');
  
    return (this.props.suggestions).filter(s => regex.test(s));
  }

  renderSuggestion = (s) => {
        return (<span>{s}</span>)
  }
  
  onChange = (event, { newValue }) => {
    this.setState({
      value: newValue
    });
    this.props.onChange(newValue);
  };

  onSuggestionsFetchRequested = ({ value }) => {
    this.setState({
      suggestions: this.getSuggestions(value)
    });
  };
 
  // Autosuggest will call this function every time you need to clear suggestions.
  onSuggestionsClearRequested = () => {
    this.setState({
      suggestions: []
    });
  };

  renderInputComponent = (props) => {
    return (
      <Input
        {...props}
        className="form-control.lg"
      />
    );
  }

  render() {
    const { value, suggestions } = this.state;
    const inputProps = {
      placeholder: 'Start typing',
      value,
      onChange: this.onChange
    };

    return (
      <Autosuggest
        renderInputComponent={this.renderInputComponent}
        suggestions={suggestions || []}
        onSuggestionsFetchRequested={this.onSuggestionsFetchRequested}
        onSuggestionsClearRequested={this.onSuggestionsClearRequested}
        getSuggestionValue={(s) => s}
        renderSuggestion={this.renderSuggestion}
        inputProps={inputProps}
      />
    );
  }
}

export default Sugestion;

