import React, { Component } from 'react';
import { TabPane } from 'reactstrap';
import PropTypes from 'prop-types';
import './styles.css';
import { connect } from 'react-redux';
const propTypes = {
  children: PropTypes.node,
};

const defaultProps = {};

class DefaultAside extends Component {

  constructor(props) {
    super(props);

    this.toggle = this.toggle.bind(this);
    this.state = {
      activeTab: '1',
    };

  }

  static contextTypes = {
    router: PropTypes.object
  }
  

  toggle(tab) {
    if (this.state.activeTab !== tab) {
      this.setState({
        activeTab: tab,
      });
    }
  }

  render() {
    // eslint-disable-next-line
    const { children, ...attributes } = this.props;
    return (
      <React.Fragment>
        <TabPane tabId="1"  className="p-3">
          <h6>Tab 1</h6>
        </TabPane>
      </React.Fragment>
    );
  }
}

DefaultAside.propTypes = propTypes;
DefaultAside.defaultProps = defaultProps;

const mapStateToProps = (state) => {
  return {};
};

const mapDispatchToProps = {}

export default connect(mapStateToProps, mapDispatchToProps)(DefaultAside);