import React, { Component } from 'react';
import { DropdownItem, DropdownMenu, DropdownToggle, Nav, NavItem, NavLink } from 'reactstrap';
import { AppAsideToggler, AppHeaderDropdown, AppNavbarBrand, AppSidebarToggler } from '@coreui/react';
import logo from '../../assets/img/brand/logo.png'
import sygnet from '../../assets/img/brand/sygnet.png'
import { Rest } from '../../services/rest.service';
import PropTypes from 'prop-types';

export class DefaultHeader extends Component {
  
  isLoggedUser = () => {
    return this.props.loggedUser && this.props.loggedUser.id;
  }

  firstLetter = () => {
    return (this.props.loggedUser.username || 'X').slice(0,1);
  }
  
  renderNotifications = () => {
    return (
      <NavItem className="d-md-down-none mr-2">
        <AppAsideToggler>
          <NavLink href="#">
          </NavLink>
        </AppAsideToggler>
      </NavItem>
    );
  }
  static contextTypes = {
    router: PropTypes.object
  }

  redirect = (target) => {
    this.context.router.history.push(target)
  }

  renderUserMenu = () => {
    return (
      <AppHeaderDropdown direction="down">
        <DropdownToggle nav>
          <span className="user">{this.firstLetter()}</span>
        </DropdownToggle>
        <DropdownMenu right style={{ right: 'auto' }}>
          <DropdownItem onClick={ Rest.logout }><i className="fa fa-lock"></i>Logout</DropdownItem>
        </DropdownMenu>
      </AppHeaderDropdown>
    );
  }

  render() {

    // eslint-disable-next-line
    const { children, ...attributes } = this.props;

    return (
      <React.Fragment>
        <AppSidebarToggler className="d-lg-none" display="md" mobile />
        <AppNavbarBrand
          full={{ src: logo, width: 89, height: 25, alt: 'Logo' }}
          minimized={{ src: sygnet, width: 30, height: 30, alt: 'Logo' }}
        />
        <AppSidebarToggler className="d-md-down-none" display="lg" />
        <Nav className="ml-auto" navbar>

          {this.isLoggedUser() && this.renderUserMenu()}
            
        </Nav>
        
      </React.Fragment>
    );
  }
}