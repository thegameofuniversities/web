import React, { Component } from 'react';
import { Redirect, Route, Switch } from 'react-router-dom';
import { Container } from 'reactstrap';

import {
  AppAside,
  AppBreadcrumb,
  AppFooter,
  AppHeader,
  AppSidebar,
  AppSidebarFooter,
  AppSidebarForm,
  AppSidebarHeader,
  AppSidebarMinimizer,
  AppSidebarNav,
} from '@coreui/react';
// sidebar nav config
import navigation from '../../_nav';
// routes config
import routes from '../../routes';
import DefaultAside from './DefaultAside';
import DefaultFooter from './DefaultFooter';
import { DefaultHeader } from './DefaultHeader';
import { connect } from 'react-redux';

class DefaultLayoutComponent extends Component {

  getNavigation = () => {
    if(!this.isUserLogged()) return this.filterAnonymous(navigation);
    return this.filterByRoles(navigation, this.props.loggedUser.roles);
  }

  isUserLogged = () => {
    return this.props.loggedUser && this.props.loggedUser.id;
  }

  filterByRoles = (navigation, roles) => {
    return { items: navigation.items.filter(e => !e.roles || e.roles.some(e => (roles || []).includes(e)))};
  }

  filterAnonymous = (navigation) => {
    return { items: navigation.items.filter(e => e.anonymous)};
  }

  render() {
    return (
      <div className="app">
        <AppHeader fixed>
          <DefaultHeader loggedUser={this.props.loggedUser}/>
        </AppHeader>
        <div className="app-body">
          <AppSidebar fixed display="lg">
            <AppSidebarHeader />
            <AppSidebarForm />
            <AppSidebarNav navConfig={this.getNavigation()} {...this.props} />
            <AppSidebarFooter />
            <AppSidebarMinimizer />
          </AppSidebar>
          <main className="main">
            <AppBreadcrumb 
            appRoutes={routes}
            />
            <Container fluid>
              <Switch>
                {routes.map((route, idx) => {
                    return route.component ? (<Route key={idx} path={route.path} exact={route.exact} name={route.name} render={props => (
                        <route.component {...props} />
                      )} />)
                      : (null);
                  },
                )}
                <Redirect from="/" to="/dashboard" />
              </Switch>
            </Container>
          </main>
          {
            this.isUserLogged() && 
            <AppAside fixed>
              <DefaultAside />
            </AppAside>
          }
        </div>
        <AppFooter>
          <DefaultFooter />
        </AppFooter>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return { loggedUser: state.loggedUser.loggedUser };
};

const mapDispatchToProps = {}

export const DefaultLayout = connect(mapStateToProps, mapDispatchToProps)(DefaultLayoutComponent);
