import React from "react";
import Loader from 'react-loader-spinner'

const AppLoader = () => {
  return (
    <>
      <Loader
        type="Oval"
        color={'red'}
        height={20}
        width={20}
      />
    </>
  );
}

export default AppLoader;