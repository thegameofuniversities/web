import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Notification } from './notification/notification';
import { CSSTransition,TransitionGroup } from 'react-transition-group';
import { removeNotification } from '../redux/actions';
import './styles.css';

class NotificationsComponent extends Component {
    render() {
        const toasts = this.props.notifications.map(e => {
            return (
                <CSSTransition  key={e.id} timeout={300} classNames="animation">
                    <Notification {...e} onClose={this.props.removeNotification}/>
                </CSSTransition>
            )
        });

        return (
            <div className='notifications'>
                <TransitionGroup>
                    {toasts}
                </TransitionGroup>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        ...state.notifications
    };
};

export const Notifications = connect(mapStateToProps, {removeNotification})(NotificationsComponent);