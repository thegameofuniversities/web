export const addNotification = ( () => {
    let notificationCounter = 0    
    
    return (notification) => ({
        type: 'ADD_NEW_NOTIFICATION',
        notification: { type:'info', timeout:10000, ...notification, id: ++notificationCounter }
    });
} )();

export const removeNotification = (id) => ({
    type: "REMOVE_NOTIFICATION",
    id
});

export const updateLoggedUser = (user) => ({
    type: "UPDATE_LOGGED_USER",
    loggedUser: user
});

export const logoutUser = () => ({
    type: "LOGOUT_USER"
});