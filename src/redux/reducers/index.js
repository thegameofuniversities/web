import { combineReducers } from 'redux';
import { notifications } from './notifications';
import { loggedUser } from './loggedUser';

export default combineReducers({
    notifications,
    loggedUser,
});