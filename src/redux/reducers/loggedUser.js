export const loggedUser = (state = {}, action) => {
    switch(action.type) {
        case 'UPDATE_LOGGED_USER':
            return {
                loggedUser: action.loggedUser
            };
        case 'LOGOUT_USER':
            return {}
        default:
            return state;
    }
}