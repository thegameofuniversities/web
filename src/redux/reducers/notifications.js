export const notifications = (state = {notifications: []}, action) => {
    switch(action.type) {
        case 'ADD_NEW_NOTIFICATION':
            return {
                notifications: [action.notification, ...state.notifications]
            };
        case 'REMOVE_NOTIFICATION':
            return {
                notifications: state.notifications.filter(n => n.id !== action.id)
            }
        default:
            return state;
    }
}