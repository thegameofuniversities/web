import { createStore, compose } from 'redux';
import reducers from './reducers';
import JwtDecode from 'jwt-decode';
import { expHandler } from '../services/tokenExpirationHandler.service';

function defaultStore() {
    const token = sessionStorage.getItem("user");
    if(!token) return {};
    const user = JwtDecode(token);
    if(user.exp < new Date().getTime()/1000) {
        return {}
    } else {
        expHandler(user);
        return { loggedUser: { loggedUser: user}};
    }
}

export const store = createStore(reducers, defaultStore(), compose(window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()));
