import React from 'react';
import Loadable from 'react-loadable'

import { DefaultLayout } from './containers/DefaultLayout';

function Loading() {
  return <></>;
}
const Dashboard = Loadable({
  loader: () => import('./views/Dashboard'),
  loading: Loading,
});

const Stats = Loadable({
  loader: () => import('./views/Stats/Stats'),
  loading: Loading
});

const GameList = Loadable({
  loader: () => import('./views/game/List/List'),
  loading: Loading,
});

const MyGames = Loadable({
  loader: () => import('./views/game/BookedGames/BookedGames'),
  loading: Loading,
});

const Game = Loadable({
  loader: () => import('./views/game/Page/Page'),
  loading: Loading,
});

const New = Loadable({
  loader: () => import('./views/game/New/New'),
  loading: Loading,
});

// https://github.com/ReactTraining/react-router/tree/master/packages/react-router-config
const routes = [
  { path: '/', exact: true, name: 'Home', component: DefaultLayout },
  { path: '/dashboard', name: 'Dashboard', component: Dashboard },
  { path: '/my-games', name: 'PLayed games', component: MyGames },
  { path: '/list', name: 'List of games', component: GameList },
  { path: '/game/:id', name: 'Game details', component: Game},
  { path: '/new-game', name: 'Add new game', component: New},
  { path: '/stats', name: 'Statystics', component: Stats },
];

export default routes;
