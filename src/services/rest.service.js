import { Config } from '../config';
import { addNotification, updateLoggedUser, logoutUser } from '../redux/actions';
import { store } from '../redux/store';
import JwtDecode from 'jwt-decode';
import { expHandler } from './tokenExpirationHandler.service';

export const Rest = {
  register,
  login,
  logout,
  getByEan,
  remindPasswordRequest,
  getGames,
  getGamesNames,
  addGame,
  getGame,
  getMonth,
  book,
  getBooked,
  getGameStats,
  getUserStats,
  getMonthDailyBookingAmount,
}

function defaultHeaders()  {
  if (!sessionStorage.getItem("user")) {
    return {
      'Content-Type': 'application/json',
    };
  } else {
    return {
      'Content-Type': 'application/json',
      Authorization: 'Bearer ' + sessionStorage.getItem("user"),
    };
  }
}

function logout() {
    window.location = window.origin+'/#/login';
    store.dispatch(logoutUser());
    sessionStorage.removeItem('user');
}

function seccessLogin(auth) {
    const token = auth.substring('Bearer '.length);
    sessionStorage.setItem("user", token);
    expHandler(JwtDecode(token));
    const user = JwtDecode(token)
    store.dispatch(updateLoggedUser({
      id: user.id,
      username: user.sub,
      roles: (user.authorities ||'').split(',')
    }));
}

function getGames(page, size, suggestion) {
  console.log(suggestion)
  const requestOptions = {
    method: 'get',
    headers: defaultHeaders(),
  }
  return request(`/game?page=${page-1}&size=${size}&query=${suggestion || ""}`, requestOptions);
}

function getBooked(page, size) {
  const requestOptions = {
    method: 'get',
    headers: defaultHeaders(),
  }
  return request(`/user/bookings?page=${page-1}&size=${size}`, requestOptions);
}

function getMonth(id, month, year) {
  const requestOptions = {
    method: 'get',
    headers: defaultHeaders(),
  }
  return request(`/bookings/${id}/${year}/${month}`, requestOptions);
}

function getGame(id) {
  const requestOptions = {
    method: 'get',
    headers: defaultHeaders(),
  }
  return request(`/game/${id}`, requestOptions);
}

function getGamesNames() {
  const requestOptions = {
    method: 'get',
    headers: defaultHeaders(),
  }
  return request(`/game/names`, requestOptions);
}

function remindPasswordRequest(email) {
  const requestOptions = {
    method: 'post',
    body: JSON.stringify({email}),
    headers: defaultHeaders(),
  }
  return request(`/remindPassword`, requestOptions);
}

function book(form) {
  const requestOptions = {
    method: 'post',
    body: JSON.stringify(form),
    headers: defaultHeaders(),
  }
  return request(`/game/book`, requestOptions);
}

function addGame(form) {
  const requestOptions = {
    method: 'post',
    body: JSON.stringify(form),
    headers: defaultHeaders(),
  }
  return request(`/game`, requestOptions);
}
function login(loginForm) {
    const requestOptions = {
        method: 'post',
        body: JSON.stringify(loginForm),
        headers: defaultHeaders()
    }
    return request('/login', requestOptions).then(user => {
        store.dispatch(addNotification({type: 'success', message: 'Loged out!'}));
        return user;
    }).catch(error => {
        store.dispatch(addNotification({type: 'danger', message: 'Could not login'}));
        return Promise.reject(error);
    });
}

function register(registerForm) {
  const requestOptions = {
    method: 'post',
    body: JSON.stringify(registerForm),
    headers: defaultHeaders()
  }
  return request('/user/register', requestOptions)
}

function testPasswordStrength(password) {
  const requestOptions = {
    method: 'post',
    body: JSON.stringify({str: password}),
    headers: defaultHeaders()
  };
  return request('/test', requestOptions)
}

function getGameStats() {
  const requestOptions = {
    method: 'get',
    headers: defaultHeaders()
  };
  return request(`/stats/total-booking-amount`, requestOptions);
}

function getUserStats() {
  const requestOptions = {
    method: 'get',
    headers: defaultHeaders()
  };
  return request(`/stats/user-total-booking-amount`, requestOptions);
}

function getMonthDailyBookingAmount(year, month) {
  const requestOptions = {
    method: 'get',
    headers: defaultHeaders()
  };
  return request(`/stats/day-booking-amount/${year}/${month}`, requestOptions);

}

function getByEan(ean) {
  const requestOptions = {
    method: 'get',
    headers: defaultHeaders()
  };
  return request(`/game/ean/${ean}`, requestOptions);
}

function request(url, options) {
    return fetch(`${Config.apiUrl}${url}`,options).then(handleResponse);
}

function handleResponse(response) {
  return response
    .text()
    .then(text => {
      const data = text && JSON.parse(text);
      if(!response.ok) {
        if(response.status === 401 || response.status === 403) {
          store.dispatch(addNotification({type:'warn', message: 'You are logged out'}));
          logout();
        }
        const error = (data && data.message) || response.statusText;
        return Promise.reject(error);
      }
      const auth = response.headers.get("Authorization");
      if(auth) {
        seccessLogin(auth);
      }
      return data;
    });
}
