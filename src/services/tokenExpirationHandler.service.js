import { Rest } from './rest.service';

export const expHandler = (function() {
    let timeout = null;
    return function(token) {
        if(timeout) {
            clearTimeout(timeout);
        }
        timeout = setTimeout(() => {
          Rest.logout();
        }, token.exp*1000 - new Date().getTime())
    }
})();