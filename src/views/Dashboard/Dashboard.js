import React, { Component } from 'react';
import { connect } from 'react-redux';
import GameWidget from '../../components/GameWidget/GameWidget'
import { img } from './exampleImg';
import { Card, CardBody, CardFooter, Row, Col } from 'reactstrap';
import Sugestion from '../../components/Sugestion/Sugestion';
import BarcodeReader from '../../components/BarcodeReader/BarcodeReader';
import GameBookingStats from '../Stats/GameBookingStats';

class Dashboard extends Component {
  
  render() {
    const games = [1,2,3,4,5,6,7,8,9,10,11,12];

    return (
      <div className="animated fadeIn">
      <Row>
        <Col md={12} xs={6} xl={6}>
        <Card>
          <CardBody>
            <img className="codeImg" src="https://upload.wikimedia.org/wikipedia/commons/thumb/3/3a/Example_barcode.svg/1280px-Example_barcode.svg.png" alt="code"/>
          </CardBody>
          <CardFooter>
            <BarcodeReader/>
          </CardFooter>
        </Card>
        </Col>
        <Col md={12} xs={6} xl={6}>
          <GameBookingStats />
        </Col>
      </Row>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return { loggedUser: state.loggedUser.loggedUser };
};

const mapDispatchToProps = {}

export default connect(mapStateToProps, mapDispatchToProps)(Dashboard);

