import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
  Button,
  Card,
  CardBody,
  CardGroup,
  Col,
  Container,
  Form,
  Input,
  InputGroup,
  InputGroupAddon,
  InputGroupText,
  Row,
} from 'reactstrap';

import { addNotification } from '../../../redux/actions';
import { Rest } from '../../../services/rest.service';
import PropTypes from 'prop-types';
import AppLoader from '../../../loader/loader';

class Login extends Component {

  constructor(props) {
    super(props);
    this.state = { username: '', password :'', logging: false};
  }
  
  static contextTypes = {
    router: PropTypes.object
  }
  
  redirect = (target) => {
    this.context.router.history.push(target);
  }
  
  handleChange = (event) => {
    const {name, value } = event.target;
    this.setState({ [name] : value});
  }

  submitLogin = (event) => {
    event.preventDefault();
    this.setState({
      ...this.state,
      logging: true,
    });
    Rest.login({
      username: this.state.username,
      password: this.state.password
    })
      .then(_ => this.redirect('/dashboard'))
      .catch(_ => {
        this.setState({
          ...this.state,
          logging: false,
        });
      });
  }

  register = (event) => {
    event.preventDefault();
    this.redirect('/register')
  }

  render() {
    const { username, password } = this.state;
    return (
      <div className="app flex-row align-items-center">
        <Container>
          <Row className="justify-content-center">
            <Col md="8">
              <CardGroup>
                <Card className="p-4">
                  <CardBody>
                    <Form onSubmit={this.submitLogin}>
                      <h1>Login</h1>
                      <InputGroup className="mb-3">
                        <InputGroupAddon addonType="prepend">
                          <InputGroupText>
                            <i className="icon-user"></i>
                          </InputGroupText>
                        </InputGroupAddon>
                        <Input
                          name="username"
                          type="text"
                          placeholder="Username" 
                          value={username}
                          onChange={this.handleChange} />
                      </InputGroup>
                      <InputGroup className="mb-4">
                        <InputGroupAddon addonType="prepend">
                          <InputGroupText>
                            <i className="icon-lock"></i>
                          </InputGroupText>
                        </InputGroupAddon>
                        <Input 
                          name="password"
                          type="password"
                          placeholder="Password" 
                          value={password}
                          onChange={this.handleChange} />
                      </InputGroup>
                      <Row>
                        <Col xs="6">
                          <Button type="submit" color="primary" className="px-4">
                            <>
                              {this.state.logging && <AppLoader/>}
                              {this.state.logging || 'Zaloguj' }
                            </>
                          </Button>
                        </Col>
                        <Col xs="6" className="text-right">
                          {/* <Button type="button" color="link" className="px-0" onClick={() => this.redirect('/remindPassword')}>Nie pamiętasz hasła?</Button> */}
                        </Col>
                      </Row>
                    </Form>
                  </CardBody>
                </Card>
                <Card className="text-white bg-primary py-5 d-md-down-none" style={{ width: 44 + '%' }}>
                  <CardBody className="text-center">
                    <div>
                      <h2>Create new account</h2>
                      {/* <span>Nielimitowany dostep do fukcji portalu.</span><br/>
                      <p><b>I wiele więcej!</b></p> */}
                      <Button color="primary" className="mt-4" active onClick={this.register}>Register just now</Button>
                    </div>
                  </CardBody>
                </Card>
              </CardGroup>
            </Col>
          </Row>
        </Container>
      </div>
    );
  }
}


const mapStateToProps = (state) => {
  return {};
};

const mapDispatchToProps = { addNotification }

export default Login = connect(mapStateToProps, mapDispatchToProps)(Login);

