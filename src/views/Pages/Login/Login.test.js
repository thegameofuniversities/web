import React from 'react';
import ReactDOM from 'react-dom';
import Login from './Login';
import { store } from '../../../redux/store';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<Login store={store} />, div);
  ReactDOM.unmountComponentAtNode(div);
});
