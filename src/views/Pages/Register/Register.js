import React, { Component } from 'react';
import {
  Button,
  Card,
  CardBody,
  Progress,
  Col,
  Container,
  Form,
  FormGroup,
  FormFeedback,
  Input,
  InputGroup,
  InputGroupAddon,
  InputGroupText,
  Row
} from 'reactstrap';
import {
  CSSTransition,
  TransitionGroup,
} from 'react-transition-group';
import { connect } from 'react-redux';
import { addNotification } from '../../../redux/actions';

import { Rest } from '../../../services/rest.service';
import { RegisterForm } from './RegisterForm';
import './style.css';

class Register extends Component {

  constructor(props){
    super(props);
    this.state = {inputs:[]}
  }

  componentDidMount()  {
    this.setState({
      inputs: RegisterForm.inputs(this)
    })
  }

  findChangedItem = name => {
    let state = { ...this.state };
    let index = state.inputs.findIndex(e => e.name === name);
    return event => {
      let method = this.changeRequiredNonEmpty;
      switch(name){
        case RegisterForm.FORM.EMAIL:
          method = this.changeEmail;
          break;
        case RegisterForm.FORM.PASSWORD:
          method = this.changePassword;
          break;
        case RegisterForm.FORM.CONFIRM_PASSWORD:
          method = this.changeRepeatPassword;
          break;
        case RegisterForm.FORM.ACADEMIC_TITLE:
          method = (name, element) => {
            element.value = name;
            return element;
          };
          break;
        default:
          method = this.changeRequiredNonEmpty;
      }
      let newItem = method(event.target.value, state.inputs[index]);
      state.inputs[index] = newItem;
      this.setState(state);
    }
  }

  changeRequiredNonEmpty = (newValue, element) => {
    element.value = newValue;
    element.correct = !!newValue.trim();
    element.message = 'Pole jest wymagane';
    return element;
  }
  
  changeEmail = (newValue, element) => {
    element = this.changeRequiredNonEmpty(newValue, element);
    if(element.correct) {
      let emailExpression = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
      element.correct = emailExpression.test(newValue.toLowerCase());
      element.message = 'Proszę podać prawidłowy adres email';
    }
    return element;
  }

  changePassword = (newValue, element) => {
    element.value = newValue;
    let repeatPassword = this.state.inputs.find(e => e.name === RegisterForm.FORM.CONFIRM_PASSWORD);
    repeatPassword.correct = repeatPassword.value === newValue;
    let index = this.state.inputs.findIndex(e => e.name === RegisterForm.FORM.CONFIRM_PASSWORD);
    let s = this.state;
    s.inputs[index] = repeatPassword;
    if(s.inputs[index].correct) {
      s.inputs[index].message = 'Passwords do not match';
    }
    this.setState(s);
    return element;
  }
  
  changeRepeatPassword = (newValue, element) => {
    let password = this.state.inputs.find(e => e.name === RegisterForm.FORM.PASSWORD);
    element.value = newValue;
    element.correct = newValue === password.value;
    if(!element.correct) {
      element.message = 'Passwords do not match';
    }
    return element;
  }

  submitRegistration = (event) => {
    event.preventDefault();
    const isCorrect = this.state.inputs.map(e => e.correct).reduce((a,b) => a && b);
    if(!isCorrect) {
      return;
    }
    const registerForm = {}
    this.state.inputs.forEach(input => {
      if(input.name === 'login') {
        registerForm.username = input.value;
      } else if(input.value) {
        registerForm[input.name] = input.value;
      }
    });
    
    Rest.register(registerForm)
    .then(data => {
      this.props.addNotification({
        type: 'success',
        message: 'Account created!',
      });
      this.props.history.push('/login');
    })
    .catch(() => {
      this.props.addNotification({
        type: 'danger',
        message: 'Could not create account!',
      });
    });
  }

  createInput = (element) => {
    return (
      <CSSTransition key={element.name} timeout={1000} classNames="message">
        <FormGroup>
          <InputGroup className="mb-3">
            <InputGroupAddon addonType="prepend">
              <InputGroupText>
                {element.icon.startsWith('icon') && <i className={element.icon}></i>}
                {element.icon.startsWith('icon') || element.icon}
              </InputGroupText>
            </InputGroupAddon>
            <Input
              invalid={!element.correct}
              type={element.secured ? "password" : "text"}
              value={element.value}
              onChange={element.onChange}
              placeholder={element.visibleAs}
            />
            { element.name === RegisterForm.FORM.PASSWORD && 
              <FormFeedback>Siła hasła
                <Progress className="progress-xs" color={element.securityLevel<50 ? "danger" : (element.securityLevel < 90) ? "success" : "warning"} value={element.securityLevel} />
              </FormFeedback>
            }
            <FormFeedback>{element.message}</FormFeedback>
          </InputGroup>
        </FormGroup>
      </CSSTransition>
      );
  }

  render() {
    const child = this.state.inputs.map( element => this.createInput(element));
    return (
      <div className="app flex-row align-items-center">
        <Container>
          <Row className="justify-content-center">
            <Col md="6">
              <Card className="mx-4">
                <CardBody className="p-4">
                  <Form onSubmit={this.submitRegistration}>
                    <h1>Register</h1>
                    <p className="text-muted">Create your own account</p>
                    <TransitionGroup>
                      {child}
                    </TransitionGroup>
                    <Button type="submit" color="success" block>Do it!</Button>
                  </Form>
                </CardBody>
              </Card>
            </Col>
          </Row>
        </Container>
      </div>
    );
  }
}


const mapStateToProps = (state) => {
  return {};
};

const mapDispatchToProps = { addNotification }

export default Register = connect(mapStateToProps, mapDispatchToProps)(Register);
