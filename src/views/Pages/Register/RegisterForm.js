export const RegisterForm = {
    FORM: {
        FIRST_NAME: 'firstName',
        LAST_NAME: 'lastName',
        LOGIN: 'login',
        EMAIL: 'email', 
        PASSWORD: 'password',
        CONFIRM_PASSWORD: 'confirmedPassword'
    },
    inputs: function(parent) {
        return [
            // {
            //     name: this.FORM.FIRST_NAME,
            //     visibleAs: 'Imię',
            //     value: '',
            //     correct: true,
            //     message: 'Pole jest wymagane',
            //     onChange: event => parent.findChangedItem(this.FORM.FIRST_NAME)(event),
            //     icon: 'icon-user'
            // },
            // {
            //     name: this.FORM.LAST_NAME,
            //     visibleAs: 'Nazwisko',
            //     value: '',
            //     correct: true,
            //     message: 'Pole jest wymagane',
            //     onChange: event => parent.findChangedItem(this.FORM.LAST_NAME)(event),
            //     icon: 'icon-user'
            // },
            {
                name: this.FORM.LOGIN,
                visibleAs: 'Login',
                value: '',
                correct: true,
                message: 'Required',
                onChange: event => parent.findChangedItem(this.FORM.LOGIN)(event),
                icon: 'icon-tag'
            },
            {
                name: this.FORM.EMAIL,
                visibleAs: 'Email',
                value: '',
                correct: true,
                message: 'It is not valid email',
                onChange: event => parent.findChangedItem(this.FORM.EMAIL)(event),
                icon: '@'
            },
            {
                name: this.FORM.PASSWORD,
                secured: true,
                visibleAs: 'Password',
                value: '',
                correct: true,
                message: 'To weak password',
                onChange: event => parent.findChangedItem(this.FORM.PASSWORD)(event),
                icon: 'icon-lock',
                securityLevel: 0
            },
            {
                name: this.FORM.CONFIRM_PASSWORD,
                secured: true,
                visibleAs: 'Password',
                value: '',
                correct: true,
                message: 'Passwords does not match',
                onChange: event => parent.findChangedItem(this.FORM.CONFIRM_PASSWORD)(event),
                icon: 'icon-lock'
            }
        ]
    }
}