import React from "react";
import GameBookingStats from "./GameBookingStats";
import UserBookingStats from "./UserBookingStats";
import MonthDailyBookingStats from "./MonthDailyBookingStats";

export default function Stats(props) {
  return (
    <>
      <GameBookingStats />
      <UserBookingStats />
      <MonthDailyBookingStats />
    </>
  );
}
