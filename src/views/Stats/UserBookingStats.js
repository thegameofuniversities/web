import React, { Component } from 'react';
import { Rest } from '../../services/rest.service';
import { Bar, Doughnut, Line, Pie, Polar, Radar } from 'react-chartjs-2';
import { Card, CardBody, CardColumns, CardHeader } from 'reactstrap';
import { CustomTooltips } from '@coreui/coreui-plugin-chartjs-custom-tooltips';

const bar = {
  labels: [],
  datasets: [
    {
      label: 'Najcz. wyp. użytkownicy',
      backgroundColor: 'rgba(255,99,132,0.2)',
      borderColor: 'rgba(255,99,132,1)',
      borderWidth: 1,
      hoverBackgroundColor: 'rgba(255,99,132,0.4)',
      hoverBorderColor: 'rgba(255,99,132,1)',
      data: null
    },
  ],
};

const options = {
  tooltips: {
    enabled: false,
    custom: CustomTooltips
  },
  scales: {
    yAxes: [{
      ticks: {
        beginAtZero: true
      }
    }]
  },
  maintainAspectRatio: false
};

class UserBookingStats extends Component {

  constructor(props) {
    super(props);

    this.state = {
      loaded: false
    }
  }

  componentDidMount() {
    this.getGameStats();
  }

  getGameStats() {
    Rest.getUserStats()
      .then(resp => {
        bar.labels = resp.map(r => r.gameName);
        bar.datasets[0].data = resp.map(r => r.amount);
        this.setState({ loaded: true });
      });
  }

  render() {
    return (
      <Card>
        <CardHeader>
          Najczęściej wypożyczający użytkownicy
        </CardHeader>
        {this.state.loaded && <CardBody>
          <div className="chart-wrapper">
            <Bar data={bar} options={options} />
          </div>
        </CardBody>}
      </Card>
    )
  }

}

export default UserBookingStats;
