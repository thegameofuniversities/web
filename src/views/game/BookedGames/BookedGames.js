import React, { Component } from 'react';
import { Row, Col } from 'reactstrap';
import GameTimeWidget from '../../../components/GameTimeWidget/GameTimeWidget';
import { PaginationComponent } from '../../../components/PaginationComponent/PaginationComponent';
import { Rest } from '../../../services/rest.service';

class BookedGames extends Component {

  constructor() {
    super();
    this.state = {
      pagination: {
        page: 1,
        size: 8,
        total: 101,
      },
      items: [],
    }
  }

  componentDidMount() {
    this.update();
  }

  pageChanged = (page) => {
    this.setState({
      ...this.state,
      pagination: {
        ...this.state.pagination,
        page
      }
    });
    this.update();
  }

  update = () => {
    Rest.getBooked(this.state.pagination.page, this.state.pagination.size)
      .then(e => {
        this.setState({
          ...this.state,
          pagination: {
            ...this.state.pagination,
            total: e.count
          },
          items: e.games,
        });
    });
  }
  render() {
      return (
        <>
          <Row>
            {this.state.items.map((i,j) => <Col  md={3} key={j}>
              <GameTimeWidget {...i.game} start={i.start} end={i.end} ></GameTimeWidget>
            </Col>)}
          </Row>
          { this.state.pagination.size <= this.state.pagination.total &&
            <PaginationComponent {...this.state.pagination} onChange={this.pageChanged}/>
          }
        </>
      );
  }
}

export default BookedGames;

