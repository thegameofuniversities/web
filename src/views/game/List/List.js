import React, { Component } from 'react';
import { Row, Col, Card, CardHeader, CardBody } from 'reactstrap';
import GameWidget from '../../../components/GameWidget/GameWidget';
import { PaginationComponent } from '../../../components/PaginationComponent/PaginationComponent';
import Sugestion from '../../../components/Sugestion/Sugestion';
import { Rest } from '../../../services/rest.service';

class GamesList extends Component {

  constructor() {
    super();
    this.state = {
      pagination: {
        page: 1,
        size: 5,
        total: 101,
      },
      suggestions: [],
      items: [],
    }
  }

  componentDidMount() {
    this.updateSuggestions();
    this.updatePage();
  }

  updateSuggestions = () => {
    Rest.getGamesNames().then(r => {
      this.setState({
        ...this.state,
        suggestions: r,
      })
    })
  }

  pageChanged = (page) => {
    this.setState({
      ...this.state,
      pagination: {
        ...this.state.pagination,
        page
      },
    });
    this.updatePage();
  }

  changeSuggesstion = (suggestion) => {
    this.setState({
      ...this.state,
      suggestion,
    });
    setTimeout(this.updatePage,100);
  }

  updatePage = () => {
    Rest.getGames(this.state.pagination.page, this.state.pagination.size, this.state.suggestion || '')
      .then(r => {
        this.setState({
          ...this.state,
          pagination: {
            ...this.state.pagination,
            total: r.count,
          },
          items: r.games,
        });
      });
  }

  render() {
    return (
      <>
        <Card>
          <CardHeader className="text-center">Find by title</CardHeader>
          <CardBody>
            <Sugestion suggestions={this.state.suggestions} onChange={this.changeSuggesstion}/>  
          </CardBody>
        </Card>
        <Row>
          {this.state.items.map(i => <Col md={4} xs={12} key={i.id}>
            <GameWidget
              {...i}
              link={"/#/game/" + i.id}
              color="primary" 
              footer={true}
            ></GameWidget>
          </Col>)}
        </Row>
        { this.state.pagination.size <= this.state.pagination.total &&
          <PaginationComponent {...this.state.pagination} onChange={this.pageChanged}/>
        }
      </>
    );
  }
}

export default GamesList;

