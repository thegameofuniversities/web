import React, { Component } from 'react';
import {
  Button,
  Card,
  CardBody,
  Col,
  Container,
  Form,
  FormGroup,
  FormFeedback,
  Input,
  InputGroup,
  InputGroupAddon,
  InputGroupText,
  Row
} from 'reactstrap';
import {
  CSSTransition,
  TransitionGroup,
} from 'react-transition-group';
import { connect } from 'react-redux';
import './style.css';
import { Rest } from '../../../services/rest.service';
import { addNotification } from '../../../redux/actions';
import PropTypes from 'prop-types';


class NewGame extends Component {

  constructor() {
    super();
    this.state = {
      title: { value: '' },
      description: { value: '' },
      image: { value: '' },
      details: { value: '' },
      count: { value: 1, correct: true },
    };
  }

  static contextTypes = {
    router: PropTypes.object
  }
  
  redirect = (target) => {
    this.context.router.history.push(target);
  }

  updateField = (fieldName) => {
    return (event) => {
      const value = event.target.value;
      this.setState({
        ...this.state,
        [fieldName]: {
          value,
          correct: !!value,
          touched: true,
          errorMessage: !!value ? undefined : 'Pole jest wymagane!'
        }
      });
    }
  }

  updateDetails = (event) => {
    const value = event.target.value;
    // const correct = /^https?:\/\/+/.test(value);
    // console.log(correct)
    this.setState({
      ...this.state,
      details: {
        value,
        correct: true,
        touched: true,
        // errorMessage: correct ? undefined : 'Wymagany adres rozpoczynający się od http:// lub https://' 
      }
    });
  }

  handleChangeImg = (files) => {
    var reader = new FileReader();
    reader.onloadend = () => {
      this.setState({
        ...this.state,
        image: {
          value: reader.result,
        },
      });
    }
    reader.readAsDataURL(files[0]); 
  }

  submitForm = (event) => {
    event.preventDefault();
    if(this.state.title.correct && this.state.count.correct && this.state.description.correct && this.state.image.value) {
      const form = {
        title: this.state.title.value,
        description: this.state.description.value,
        image: this.state.image.value,
        count: this.state.count.value
      };
      Rest.addGame(form)
        .then(r => {
          this.redirect('/game/'+ r.id)
          this.props.addNotification({ type: 'info', message: 'New game was added!'});
        })
        .catch(e =>{
          this.props.addNotification({ type: 'info', message: 'Could not add new game.'});
        });
    } else {
      this.props.addNotification({ type: 'info', message: 'First of all, please fill the form.'});
    }
  }
  render() {
      return (
        <>
        <div className="app flex-row align-items-center">
        <Container>
          <Row className="justify-content-center">
            <Col md="6">
              <Card className="mx-4">
                <CardBody className="p-4">
                  <Form onSubmit={this.submitForm}>
                    <h1>Add new game</h1>
                    <TransitionGroup>
                      <CSSTransition timeout={1000} classNames="message">
                        <FormGroup>
                          <InputGroup className="mb-3">
                            <InputGroupAddon addonType="prepend">
                              <InputGroupText>
                                <i>T</i>
                              </InputGroupText>
                            </InputGroupAddon>
                            <Input
                              invalid={!this.state.title.correct && this.state.title.touched}
                              type="text"
                              value={this.state.title.value}
                              onChange={this.updateField('title')}
                              placeholder="Title"
                            />
                            <FormFeedback>{this.state.title.errorMessage}</FormFeedback>
                          </InputGroup>
                        </FormGroup>
                      </CSSTransition>
                      <CSSTransition timeout={1000} classNames="message">
                        <FormGroup>
                          <InputGroup className="mb-3">
                            <InputGroupAddon addonType="prepend">
                              <InputGroupText>
                                <i>D</i>
                              </InputGroupText>
                            </InputGroupAddon>
                            <Input
                              type="textarea"
                              invalid={!this.state.description.correct && this.state.description.touched}
                              value={this.state.description.value}
                              onChange={this.updateField('description')}
                              placeholder="Description"
                            />
                            <FormFeedback>{this.state.description.errorMessage}</FormFeedback>
                          </InputGroup>
                        </FormGroup>
                      </CSSTransition>
                      <CSSTransition timeout={1000} classNames="message">
                        <FormGroup>
                          <InputGroup className="mb-3">
                            <InputGroupAddon addonType="prepend">
                              <InputGroupText>
                                <i>A</i>
                              </InputGroupText>
                            </InputGroupAddon>
                            <Input
                              type="number"
                              invalid={!this.state.count.correct && this.state.count.touched}
                              value={this.state.count.value}
                              onChange={this.updateField('count')}
                              placeholder="Amount"
                            />
                            <FormFeedback>{this.state.count.errorMessage}</FormFeedback>
                          </InputGroup>
                        </FormGroup>
                      </CSSTransition>
                      <CSSTransition timeout={1000} classNames="message">
                        <FormGroup>
                          <InputGroup className="mb-3">
                            <InputGroupAddon addonType="prepend">
                              <InputGroupText>
                                <i>U</i>
                              </InputGroupText>
                            </InputGroupAddon>
                            <Input
                              type="text"
                              invalid={!this.state.details.correct && this.state.details.touched}
                              value={this.state.details.value}
                              onChange={this.updateDetails}
                              placeholder="Url to details page"
                            />
                            <FormFeedback>{this.state.details.errorMessage}</FormFeedback>
                          </InputGroup>
                        </FormGroup>
                      </CSSTransition>
                      <CSSTransition timeout={1000} classNames="message">
                        <FormGroup row>
                          <Col xs="12" md="12">
                            <input id="fileinput" type="file" accept=".png" ref={(ref) => this.upload = ref} style={{ display: 'none' }} onChange={ (e) => this.handleChangeImg(e.target.files) }/>
                            <Button type="button" className="btn btn-block btn-success mb-2" onClick={()=>this.upload.click()}> Select image</Button> 
                          </Col>
                          {this.state.image.value && 
                            <Row>
                              <Col>
                              <img className="img-new" src={this.state.image.value} alt="AAAA"/>
                              </Col>
                            </Row>
                          }
                          </FormGroup>      
                        </CSSTransition>         
                    </TransitionGroup>
                    <Button type="submit" color="success" block>To it!</Button>
                  </Form>
                </CardBody>
              </Card>
            </Col>
          </Row>
        </Container>
      </div>
        </>
      );
  }
}

const mapStateToProps = (state) => {
  return {};
};

const mapDispatchToProps = { addNotification }

export default NewGame = connect(mapStateToProps, mapDispatchToProps)(NewGame);

