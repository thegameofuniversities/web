import React, { Component } from 'react';
import GameView from '../../../components/GameView/GameView';
import { connect } from 'react-redux';
import { Rest } from '../../../services/rest.service';

class Game extends Component {

  constructor() {
    super();
    this.state = {
      game: {}
    }
  }

  componentDidMount() {
    this.getGame(this.props.match.params.id);
  }
  componentDidUpdate(pop) {
    if(this.props.match.id !== pop.match.id) {
      this.getGame(this.props.match.id);
    }
  }

  getGame = (id) => {
    Rest.getGame(id)
      .then(r => {
        this.setState({game: r})
      });
  }
  render() {
    console.log(this.props)
      return (
        <>
          {this.state.game.id && <GameView {...this.state.game} loggedUser={this.props.loggedUser} year={2019} month={3}></GameView>}
        </>
      );
  }
}


const mapStateToProps = (state) => {
  return { loggedUser: state.loggedUser.loggedUser };
};

const mapDispatchToProps = {}

export default connect(mapStateToProps, mapDispatchToProps)(Game);
